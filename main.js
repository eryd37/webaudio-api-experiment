 
let tones = [
  659.0, 661.0, 660.0, 510.0, 660.0, 770.0, 380.0, 510.0, 380.0, 320.0, 440.0,
  480.0, 450.0, 430.0, 380.0, 660.0, 760.0, 860.0, 700.0, 760.0, 660.0, 520.0,
  580.0, 480.0, 510.0, 380.0, 320.0, 440.0, 480.0, 450.0, 430.0, 380.0, 660.0,
  760.0, 860.0, 700.0, 760.0, 660.0, 520.0, 580.0, 480.0, 500.0, 760.0, 720.0,
  680.0, 620.0, 650.0, 380.0, 430.0, 500.0, 430.0, 500.0, 570.0, 500.0, 760.0,
  720.0, 680.0, 620.0, 650.0, 1020.0, 1020.0, 1020.0, 380.0, 500.0, 760.0,
  720.0, 680.0, 620.0, 650.0, 380.0, 430.0, 500.0, 430.0, 500.0, 570.0, 585.0,
  550.0, 500.0, 380.0, 501.0, 499.0, 500.0, 501.0, 499.0, 500.0, 501.0, 580.0,
  660.0, 870.0, 760.0, 500.0, 501.0, 499.0, 500.0, 580.0, 660.0, 660.0, 660.0,
  510.0, 660.0, 770.0, 380.0, 380.0,
];

let startTimes = [
  0.0, 0.15, 0.45, 0.75, 0.85, 1.15, 1.7, 2.375, 2.825, 3.225, 3.725, 4.025,
  4.355, 4.505, 4.805, 5.005, 5.205, 5.355, 5.655, 5.805, 6.155, 6.455, 6.605,
  6.755, 7.255, 7.705, 8.105, 8.605, 8.905, 9.235, 9.385, 9.685, 9.885, 10.085,
  10.235, 10.535, 10.685, 11.035, 11.335, 11.485, 11.635, 12.135, 12.435,
  12.535, 12.685, 12.835, 13.135, 13.435, 13.585, 13.735, 14.035, 14.185,
  14.285, 14.505, 14.805, 14.905, 15.055, 15.205, 15.505, 15.805, 16.105,
  16.255, 16.555, 16.855, 17.155, 17.255, 17.405, 17.555, 17.855, 18.155,
  18.305, 18.455, 18.755, 18.905, 19.005, 19.425, 19.875, 20.295, 20.655,
  20.955, 21.255, 21.405, 21.705, 21.855, 22.155, 22.505, 22.655, 22.805,
  23.355, 23.68, 24.28, 24.43, 24.73, 25.08, 25.23, 25.58, 25.73, 26.03, 26.33,
  26.43, 26.73, 27.28, 27.855,
];

function playTone(
  context,
  tone,
  start,
  nextStart
) {
  const oscillator = context.createOscillator();
  oscillator.frequency.value = tone;
  oscillator.type = "sine";
  oscillator.connect(context.destination);
  oscillator.start(start);
  oscillator.stop(nextStart - 0.01);
}

function createMarioSong() {
  const context = new AudioContext();
  tones.forEach((tone, index) => {
    const start = startTimes[index];
    const nextStart =
      index < startTimes.length - 1 ? startTimes[index + 1] : start + 0.5;
    playTone(context, tone, startTimes[index], nextStart);
  });
}

createMarioSong();
